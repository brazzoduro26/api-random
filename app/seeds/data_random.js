const dream = require('dreamjs');

dream.customType('genero', /hombre|mujer/);
dream.schema({genero: 'genero', edad: 'age', region: /^1[1-6]|[1-9]/});

function random () {
  return dream.generateRnd(100).output();
}

module.exports = random();
