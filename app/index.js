const express = require('express');
const morgan = require('morgan');
const app = express();
const routes = require('./routes');
const global = require('./config');

app.set('appName', 'API - Data Random');
app.set('dbName', global.db_name);
app.set('serverName', global.server_name);
app.set('portServer', global.server_port);

app.use(morgan('dev'));
app.use(routes);

app.listen(global.server_port, function () {
  console.log('\x1Bc');
  console.log('Name: ' + app.get('appName'));
  console.log('Database Name: ' + app.get('dbName'));
  console.log('Server: http://' + app.get('serverName') + ':' + app.get('portServer'));
  console.log('Server started and waiting for application at port ' + global.server_port + '!');
  console.log('\n To STOP press ctrl + c');
  console.log('-----------------------------------------------------------');
});
